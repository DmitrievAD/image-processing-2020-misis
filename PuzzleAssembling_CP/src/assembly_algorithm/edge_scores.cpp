#include <tuple>
#include <map>
#include <set>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include "edge_scores.h"
#include "utilities.h"

using std::map;
using std::tuple;
using std::set;
using std::tie;
using cv::Rect;
using cv::Mat;
using cv::Scalar;
using cv::Vec3b;

using rgb_coords = std::tuple<int, int, int>;
using piece_ref = std::tuple<int, int>;
using sparse_rgb_space = map<rgb_coords, set<piece_ref>>;
using edge_def = tuple<double, int, int, int, int>;


EdgeDistancesContainer::EdgeDistancesContainer() {}

void EdgeDistancesContainer::add_result(int first_ind, int first_rot, int second_ind, int second_rot, double dist) {
    first_indexes.push_back(first_ind);
    first_rotations.push_back(first_rot);
    second_indexes.push_back(second_ind);
    second_rotations.push_back(second_rot);
    distances.push_back(dist);
}

double find_second_smalest(const vector<double>& vect) {
    double smalest = MAXFLOAT;
    double second_smalest = MAXFLOAT;
    for (const auto& elem : vect) {
        if (elem < smalest) {
            second_smalest = smalest;
            smalest = elem;
        } else if (elem > smalest && elem < second_smalest) {
            second_smalest = elem;
        }
    }
    return second_smalest;
}

void EdgeDistancesContainer::divide_distances_by_second_smalest(int target_ind, int target_rot) {
    vector<double> dists;
    for (int i = 0; i < distances.size(); i++) {
        if (first_indexes[i] != target_ind || first_rotations[i] != target_rot) {
            continue;
        }
        dists.push_back(distances[i]);
    }
    double second_smalest = find_second_smalest(dists);
    //std::cout << "Second smalest: ";
    //std::cout << second_smalest << std::endl;
    for (int i = 0; i < distances.size(); i++) {
        if (first_indexes[i] != target_ind || first_rotations[i] != target_rot) {
            continue;
        }
        distances[i] += 0.000000001;
        distances[i] /= second_smalest;
    }
}

int EdgeDistancesContainer::size() {
    return distances.size();
}

bool EdgeDistancesContainer::is_empty() {
    return heap_form.size() == 0;
}

void EdgeDistancesContainer::heapify() {
    heap_form.clear();
    for (int i = 0; i < size(); i++) {
        heap_form.push_back(tie(distances[i], first_indexes[i], second_indexes[i], first_rotations[i], second_rotations[i]));
    }
    std::make_heap(heap_form.begin(), heap_form.end(), std::greater<edge_def>{});
}

edge_def EdgeDistancesContainer::heappop() {
    std::pop_heap(heap_form.begin(), heap_form.end(), std::greater<edge_def>{});
    edge_def result = heap_form.back();
    heap_form.pop_back();
    return result;
}


Rect get_one_column_rect(int column_num, int height) {
    return Rect(column_num, 0, 1, height);
}

rgb_coords build_point(const ImagePiece& img, const Rect& right_pixels_mask, int bucket_size, int rotation) {

    Mat pix = img.get_rotated_pix(rotation);
    // Compute sum over each channel independently
    Scalar color_sum = cv::sum(pix(right_pixels_mask));
    // Note that opencv uses reverse letters order, consequently RGB -> BGR in opencv
    int b = color_sum[2] / bucket_size;
    int g = color_sum[1] / bucket_size;
    int r = color_sum[0] / bucket_size;
    return tie(r, g, b);
}

void add_neighbour(const ImagePiece& img, const Rect& right_pixels_mask, sparse_rgb_space& image_space, int i, int bucket_size, int rotation) {
    // Point in rgb space
    rgb_coords point = build_point(img, right_pixels_mask, bucket_size, rotation);
    // We can make a reference to any piece by its index in CutImage and rotations count
    piece_ref part = tie(i, rotation);
    // If image_space already contains point
    if (image_space.count(point) > 0) {
        image_space.at(point).insert(part);
    } else {
        set<piece_ref> container;
        container.insert(part);
        image_space.emplace(point, container);
    }
}

void build_sparse_rgb_space(const CutImage& image, int bucket_size, sparse_rgb_space& image_space, bool type2) {
    // Mask to select all pixels of right border
    Rect right_pixels_mask = get_one_column_rect(image.get_square_size() - 1, image.get_square_size());

    for (int i = 0; i < image.get_pieces_count(); i++) {
        ImagePiece img = image.get(i);
        for (int rotation = 0; rotation < 4; rotation++) {
            add_neighbour(img, right_pixels_mask, image_space, i, bucket_size, rotation);
        }
    }
}

set<piece_ref> find_neighbours(const sparse_rgb_space& space, const rgb_coords& point, int num_neighbours) {
    vector<rgb_coords> points;
    for (auto [key_point, neighbours] : space) {
        points.push_back(key_point);
    }
    auto position_pointer = std::lower_bound(points.begin(), points.end(), point);
    auto left_pointer = position_pointer;
    auto right_pointer = position_pointer + 1;
    set<piece_ref> result;
    while (result.size() < num_neighbours) {

        set<piece_ref> lower_tmp = space.at(*left_pointer);
        result.insert(lower_tmp.begin(), lower_tmp.end());
        if (right_pointer < points.end()) {
            set<piece_ref> greater_tmp = space.at(*right_pointer);
            result.insert(greater_tmp.begin(), greater_tmp.end());
        }
        if (left_pointer > points.begin()) {
            left_pointer--;
        }
        if (right_pointer < (points.end() - 1)) {
            right_pointer++;
        }
        if (left_pointer == points.begin() && right_pointer >= (points.end() - 1)) {
            break;
        }
    }
    if (result.size() < num_neighbours) {
        std::cout << "WARNING: For point (" 
                    << std::get<0>(point) << ", " << std::get<1>(point) << ", " << std::get<2>(point) 
                      << ") returned less neighbours than expected" << std::endl;
    }
    return result;
}

MatrixXd column_to_matrix(const Mat& col) {
    MatrixXd m1(col.rows, 3);
    for (int i = 0; i < col.rows; i++) {
        auto scalar = col.at<cv::Vec3b>(i, 0);
        m1(i, 0) = (int)scalar[2]; // red
        m1(i, 1) = (int)scalar[1]; // green
        m1(i, 2) = (int)scalar[0]; // blue
    }
    return m1;
}

void compute_grad(const Mat& pix1, const Mat& pix2, MatrixXd& res) {
    Rect col_right = get_one_column_rect(pix1.cols - 1, pix1.rows);
    Rect col_left = get_one_column_rect(0, pix1.rows);
    // cv doesn't allow to get correct result of substraction if result is beyond the border [0, 255]
    MatrixXd pix1_col_right = column_to_matrix(pix1(col_right));
    MatrixXd pix2_col_left = column_to_matrix(pix2(col_left));
    res = pix1_col_right - pix2_col_left;
}

double get_ssd_compat(const Mat& pix1, const Mat& pix2) {
    MatrixXd grad;
    compute_grad(pix1, pix2, grad);
    return grad.array().square().sum();
}

double get_nonsym_compat(const MatrixXd& grad_matr, const Vector3d& mean, const Matrix3d& covar_inv) {
    MatrixXd centred = grad_matr.rowwise() - mean.transpose();
    MatrixXd res_m = (centred * covar_inv).array() * centred.array();//dlib::sum(dlib::pointwise_multiply(centred * covar_inv, centred));
    return res_m.sum();
}

double get_compat(const Vector3d& left_mean, const Matrix3d& left_covar_inv, 
                  const Vector3d& right_mean, const Matrix3d& right_covar_inv, 
                  const Mat& pix1, const Mat& pix2) {
    MatrixXd grad;
    compute_grad(pix1, pix2, grad);
    double comp = get_nonsym_compat(-grad, left_mean, left_covar_inv) + get_nonsym_compat(grad, right_mean, right_covar_inv);
    return comp;
}

EdgeDistancesContainer compute_edge_dists(const CutImage& image, int num_buckets, int min_neighbours_num, bool type2, bool ssd, bool div_by_sec_smalest) {
    int image_pieces_count = image.get_pieces_count();
    int bucket_size = (256 * image.get_square_size()) / num_buckets;

    sparse_rgb_space image_space; 
    build_sparse_rgb_space(image, bucket_size, image_space, type2);

    EdgeDistancesContainer result;
    int i = 1;

    set<piece_ref> all_pieces;
    for (const auto& [point, value] : image_space) {
        all_pieces.insert(value.begin(), value.end());
    }

    //for (auto [point, value] : image_space) {

        //set<piece_ref> neighbours = find_neighbours(image_space, point, min_neighbours_num);

        for (auto neighbour1 : all_pieces) {

            int sq1_ind = std::get<0>(neighbour1);
			int sq1_rot = std::get<1>(neighbour1);

			ImagePiece sq1 = image.get(sq1_ind);
			Mat pix1 = sq1.get_rotated_pix(sq1_rot);
			Vector3d left_mean1 = sq1.get_left_mean(sq1_rot);
			Matrix3d left_covar_inv1 = sq1.get_left_cov_inv(sq1_rot);

            //Vector3d distances;
            for (auto neighbour2 : all_pieces) {

                int sq2_ind = std::get<0>(neighbour2);
			    int sq2_rot = std::get<1>(neighbour2);

                if (sq1_ind == sq2_ind || ((!type2) && sq1_rot != sq2_rot)) {
                    continue;
                }

			    ImagePiece sq2 = image.get(sq2_ind);
			    Mat pix2 = sq2.get_rotated_pix(sq2_rot);
			    Vector3d right_mean2 = sq2.get_right_mean(sq2_rot);
			    Matrix3d right_covar_inv2 = sq2.get_right_cov_inv(sq2_rot);

                double d = 0;
                if (ssd) {
                    d = get_ssd_compat(pix1, pix2);
                } else {
                    d = get_compat(left_mean1, left_covar_inv1, right_mean2, right_covar_inv2, pix1, pix2);
                }
                //distances.push_back(d);
                result.add_result(sq1_ind, sq1_rot, sq2_ind, sq2_rot, d);
                //result.add_result(sq2_ind, sq2_rot, sq1_ind, sq1_rot, d);
            }

            if (div_by_sec_smalest) {
                result.divide_distances_by_second_smalest(sq1_ind, sq1_rot);
            }

            //std::cout << "Neighbour " << i <<  " processed" << std::endl;
            i++;
        }
    //}
    return result;
}

bool return_zero_condition(int neigh_flag, int& n_x, int& n_y, int W, int H) {
    if (neigh_flag == 0) {
        n_x += 1;
        if (n_x >= W) {
            return true;
        }
    } else if (neigh_flag == 1) {
        n_y -= 1;
        if (n_y < 0) {
            return true;
        }
    } else if (neigh_flag == 2) {
        n_x -= 1;
        if (n_x < 0) {
            return true;
        }
    } else if (neigh_flag == 3) {
        n_y += 1;
        if (n_y >= H) {
            return true;
        }
    } else {
        throw std::invalid_argument("Flag parameter must be in range [0, 4)");
    }
    return false;
}

double get_edge_cost(int x, int y, const TrimArray& index_grid, int extra_ind, int extra_rot, int neigh_flag, const CutImage& img, bool ssd) {
    int n_x = x;
    int n_y = y;

    int H = index_grid.get_num_rows();
    int W = index_grid.get_num_columns();

    bool return_zero = return_zero_condition(neigh_flag, n_x, n_y, W, H);
    if (return_zero) {
        return 0;
    }
    int neighb_ind = index_grid.get_index(n_y, n_x);
    if (neighb_ind < 0) {
        return 0;
    }

    int neigh_rot_n = py_mod(index_grid.get_rot(n_y, n_x) + neigh_flag, 4);
    int extra_rot_n = py_mod(extra_rot + neigh_flag, 4);

    ImagePiece sq_extra = img.get(extra_ind);
    ImagePiece sq_neigh = img.get(neighb_ind);

    Mat extra_pixels = sq_extra.get_rotated_pix(extra_rot_n);
    Mat neigh_pixels = sq_neigh.get_rotated_pix(neigh_rot_n);

    if (ssd) {
        return get_ssd_compat(extra_pixels, neigh_pixels);
    }

    Vector3d left_mean = sq_extra.get_left_mean(extra_rot_n);
    Vector3d right_mean = sq_neigh.get_right_mean(neigh_rot_n);

    Matrix3d left_cov_inv = sq_extra.get_left_cov_inv(extra_rot_n);
    Matrix3d right_cov_inv = sq_neigh.get_right_cov_inv(neigh_rot_n);

    return get_compat(left_mean, left_cov_inv, right_mean, right_cov_inv, extra_pixels, neigh_pixels);
}