#include <opencv2/core.hpp>
#include <algorithm>
#include <functional>
#include <vector>
#include <tuple>
#include <set>
#include <string>
#include <iostream>
#include <experimental/filesystem>

#include "picture_assembler.h"
#include "edge_scores.h"
#include "cut_image.h"
#include "disjoint_set_forest.h"
#include "utilities.h"

using std::tuple;
using std::get;
using std::set;
using std::string;
using cv::Mat;
using edge_def = tuple<double, int, int, int, int>;

namespace fs = std::experimental::filesystem;


void show_pieces(const CutImage& img, int ind1, int ind2, int rot1, int rot2) {
    Mat mat_1 = img.get(ind1).get_rotated_pix(rot1);
    Mat mat_2 = img.get(ind2).get_rotated_pix(rot2);
    Mat res;
    cv::hconcat(mat_1, mat_2, res);
    cv::imshow("res" + std::to_string(ind1) + std::to_string(ind2) + std::to_string(rot1) + std::to_string(rot2), res);
}

void write_picture(string path, const Mat picture) {
    Mat tmp;
    cv::cvtColor(picture, tmp, cv::COLOR_RGB2BGR);
    cv::imwrite(path, tmp);
}

set<std::pair<int, int>> get_neighbours_indexes(int i, int j, const MatrixXi& matrix, const MatrixXi& rot_matr) {
    set<std::pair<int, int>> neighbours;
    if (i > 0) {
        neighbours.insert(std::make_pair(matrix(i-1, j), rot_matr(i-1, j)));
    }
    if (j > 0) {
        neighbours.insert(std::make_pair(matrix(i, j-1), rot_matr(i, j-1)));
    } 
    if (i < matrix.rows() - 1) {
        neighbours.insert(std::make_pair(matrix(i+1, j), rot_matr(i+1, j)));
    }
    if (j < matrix.cols() - 1) {
        neighbours.insert(std::make_pair(matrix(i, j+1), rot_matr(i, j+1)));
    }
    return neighbours;
}

MatrixXi transform_indexes_into_ids(const MatrixXi& indexes, const CutImage& img) {
    MatrixXi result(indexes.rows(), indexes.cols());
    for (int i = 0; i < indexes.rows(); i++) {
        for (int j = 0; j < indexes.cols(); j++) {
            result(i, j) = indexes(i, j) >= 0 ? img.get(indexes(i, j)).get_idx() : -1;
        }
    }
    return result;
}

double compute_neighbour_metric(const CutImage& img, TrimArray final_trim_array) {

    MatrixXi true_indexes = img.true_indexes;
    MatrixXi true_rots = img.true_rotations;

    MatrixXi trim_ids = transform_indexes_into_ids(final_trim_array.index_grid, img);

    double score = 0;
    for (int i = 0; i < true_indexes.rows(); i++) {
        for (int j = 0; j < true_indexes.cols(); j++) {

            int index1 = true_indexes(i, j);
            set<std::pair<int, int>> neighbours_true = get_neighbours_indexes(i, j, true_indexes, true_rots);

            for (int i1 = 0; i1 < final_trim_array.get_num_rows(); i1++) {
                for (int j1 = 0; j1 < final_trim_array.get_num_columns(); j1++) {
                    if (trim_ids(i1, j1) == index1) {

                        int rot_difference = true_rots(i, j) - final_trim_array.get_rot(i1, j1); // if image was rotated while assembling
                        set<std::pair<int, int>> neighbours_est = get_neighbours_indexes(i1, j1, trim_ids, final_trim_array.rot_grid);

                        double local_score = 0;
                        for (const auto& n : neighbours_true) {
                            for (const auto& n_e : neighbours_est) {
                                if (n_e.first == n.first && py_mod(n_e.second + rot_difference, 4) == n.second) {
                                    local_score += 1;
                                    break;
                                }
                            }
                        }
                        score += local_score / neighbours_true.size();
                    }
                }
            }
        }
    }

    return score / true_indexes.size();
}

std::pair<Mat, double> assemble_picture(const CutImage& img, int num_buckets, int min_neigh, bool type2, bool ssd, bool div_by_sec_small, const string& save_dir) {

    // STEP 1

    int frame_H = get<0>(img.get_size());
    int frame_W = get<1>(img.get_size());

    min_neigh = 2 * frame_H * frame_W - frame_W - frame_H;

    int pieces_num = img.get_pieces_count();

    EdgeDistancesContainer distances = compute_edge_dists(img, num_buckets, min_neigh, type2, ssd, div_by_sec_small);

    vector<edge_def> true_connection = img.get_true_connections();

    distances.heapify();

    auto forest = DisjointSetForest(pieces_num);

    int clust_index = -1;
    while (forest.get_num_clusters() > 1) {
        if (distances.is_empty()) {
            std::cout << distances.size();
            throw std::invalid_argument("Out of edges");
        }
        edge_def edge = distances.heappop();
        clust_index = forest.forest_union(get<1>(edge), get<2>(edge), get<3>(edge), get<4>(edge));
        if (clust_index != -1) {
            // preventive reconstruction here
        }
    }

    forest.collapse();

    std::cout << "Num Clusters: " << forest.get_num_clusters() << std::endl 
              << "Size of piece_cord_map:  " << forest.piece_coord_map.size() << std::endl;

    Mat pre_result = forest.reconstruct(clust_index, img);
    write_picture(save_dir + "pre_trim_reconst.png", pre_result);

    // STEP 2
    
    TrimArray index_grid = forest.get_orig_trim_array();
    //Mat pixels = forest.reconstruct_trim(index_grid, img);

    TrimArray trim_ind_grid = forest.trim(index_grid, frame_W, frame_H);
    Mat trim_pixels = forest.reconstruct_trim(trim_ind_grid, img);
    write_picture(save_dir + "rec_trim_aft_trim_pix.png", trim_pixels);

    // STEP 3

    TrimArray filed_ind_grid = forest.fill(trim_ind_grid, img, ssd);
    Mat filed_pixels = forest.reconstruct_trim(filed_ind_grid, img);
    write_picture(save_dir + "filed_pix.png", filed_pixels);

    int top_tier_holes = forest.get_top_tier_holes(filed_ind_grid).size();
    int extra_pieces_count = filed_ind_grid.get_extra_pieces_size();

    if (top_tier_holes > 0 && extra_pieces_count == 0) {
        std::cout << "Assembly finished with remaining holes" << std::endl;
    } else if (top_tier_holes == 0 && extra_pieces_count > 0) {
        std::cout << "Extra pieces remain" << std::endl;
        std::cout << "List of remaining piece names: ";
        set<int> ids;
        for (int i = 0; i < extra_pieces_count; i++) {
            int id = filed_ind_grid.get_extra_piece(i);
            int parent_id = img.get_parent_piece_id(id);
            ids.insert(parent_id);
        }
        for (const int& id : ids) {
            std::cout << id << " ";
        }
        std::cout << std::endl; 
    }

    double score = compute_neighbour_metric(img, filed_ind_grid);

    return std::make_pair(filed_pixels, score);
}