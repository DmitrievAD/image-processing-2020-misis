#include <Eigen/Core>
#include <iostream>

#include "trim_array.h"

using Eigen::MatrixXi;

TrimArray::TrimArray(int rows, int cols) {
    index_grid = -1 * MatrixXi::Ones(rows, cols);
    rot_grid = -1 * MatrixXi::Ones(rows, cols);
    occupied_grid = MatrixXi::Zero(rows, cols);
}

void TrimArray::validate_get(int row, int col) const {
    if (row < 0 || row >= index_grid.rows() || col < 0 || col >= index_grid.cols()) {
        std::cout << "Row: " << row << std::endl;
        std::cout << "Max row: " << index_grid.rows() << std::endl;
        std::cout << "Column: " << col << std::endl;
        std::cout << "Max column: " << index_grid.cols() << std::endl;
        throw std::invalid_argument("Assertion row > 0 && row < max_num_rows && col > 0 && col < max_num_cols failed");
    }
}

void TrimArray::set_index(int row, int col, int ind) {
    validate_get(row, col);
    index_grid(row, col) = ind;
    if (ind > 0) {
        occupied_grid(row, col) = 1;
    }
}

void TrimArray::set_rot(int row, int col, int rot) {
    validate_get(row, col);
    if (rot < 0 || rot > 3) {
        throw std::invalid_argument("Rot argument should be in range [0, 4)");
    }
    rot_grid(row, col) = rot;
}

int TrimArray::get_index(int row, int col) const {
    validate_get(row, col);
    return index_grid(row, col);
}

int TrimArray::get_rot(int row, int col) const {
    validate_get(row, col);
    int rot = rot_grid(row, col);
    return rot;
}

int TrimArray::get_occupied(int row, int col) const {
    validate_get(row, col);
    return occupied_grid(row, col);
}

int TrimArray::get_num_rows() const {
    return index_grid.rows();
}

int TrimArray::get_num_columns() const {
    return index_grid.cols();
}

void TrimArray::validate_slice(int row, int col, int height, int width) const {
    if (row < 0 || row + height > index_grid.rows() || col < 0 || col + width > index_grid.cols()) {
        throw std::invalid_argument("Assertion (row >= 0 && row + height <= max_num_rows && col >= 0 && col + width <= max_num_cols) failed");
    }
}

int TrimArray::get_occupied_count(int row, int col, int height, int width) const {
    validate_slice(row, col, height, width);
    return occupied_grid.block(row, col, height, width).sum();
}

int TrimArray::get_extra_piece(int ind) const {
    if (ind >= extra_pieces_list.size() || ind < 0) {
        throw std::invalid_argument("Out of size of extra_piece array");
    }
    return extra_pieces_list.at(ind);
}

void TrimArray::add_extra_piece(int piece_ind) {
    extra_pieces_list.push_back(piece_ind);
}

void TrimArray::remove_extra_piece(int extra_value) {
    auto pointer = std::find(extra_pieces_list.begin(), extra_pieces_list.end(), extra_value);
    if (pointer == extra_pieces_list.end()) {
        return;
    }
    extra_pieces_list.erase(pointer);
}

int TrimArray::get_extra_pieces_size() const {
    return extra_pieces_list.size();
}