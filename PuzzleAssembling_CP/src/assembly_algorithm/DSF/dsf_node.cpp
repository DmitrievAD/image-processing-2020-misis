#include <vector>
#include <iostream>
#include <Eigen/Core>

#include "dsf_node.h"

using Eigen::Vector2i;

using namespace std;

DSFNode::DSFNode(int i) : piece_index(i) {
    Vector2i vect;
    vect << 0, 0;
    local_coords = vect;
    local_rot = 0;
    cluster_size = 1;
    id = i;
    parent_id = i;
}

int DSFNode::get_id() const {
    return id;
}

int DSFNode::get_piece_index() const {
    return piece_index;
}

int DSFNode::get_parent_id() const {
    return parent_id;
}

int DSFNode::get_local_rot() const {
    return local_rot;
}

Vector2i DSFNode::get_local_coords() const {
    return local_coords;
}

int DSFNode::get_cluster_size() const {
    return cluster_size;
}

void DSFNode::set_id(int new_id) {
    id = new_id;
}

//void DSFNode::set_piece_index(int new_piece_index) {
//    piece_index = new_piece_index;
//}

void DSFNode::set_local_rot(int new_local_rot) {
    if (new_local_rot > 3 || new_local_rot < 0) {
        throw invalid_argument("local_rot must be in range [0, 4), not " + std::to_string(new_local_rot));
    }
    local_rot = new_local_rot;
}

void DSFNode::set_local_coords(Vector2i new_local_coords) {
    local_coords = new_local_coords;
}

void DSFNode::set_parent_id(int new_parent_id) {
    parent_id = new_parent_id;
}

void DSFNode::append_to_cluster_size(int cluster_size_increasing) {
    if (cluster_size_increasing < 0) {
        throw std::invalid_argument("cluster_size_increasing must be greater than 0");
    }
    cluster_size += cluster_size_increasing;
}

DSFNode& DSFNode::operator=(const DSFNode& right) {
    set_parent_id(right.get_parent_id());
    set_local_rot(right.get_local_rot());
    set_local_coords(right.get_local_coords());
    cluster_size = right.get_cluster_size() > 0 
        ? right.get_cluster_size()
        : throw std::invalid_argument("right node cluster size must be greate than 0");
    set_id(right.get_id());
    piece_index = right.get_piece_index();
    std::cout << "Piece index of node " << id << " was changed to " << piece_index << std::endl;
}
