#include <vector>
#include <map>
#include <iostream>
#include <Eigen/Core>

#include "dsf_node.h"
#include "disjoint_set_forest.h"
#include "utilities.h"
#include "edge_scores.h"

using Eigen::Vector2i;
using Eigen::Matrix2i;
using std::vector;

SearchResult::SearchResult(int parent_id, int parRot, MatrixXi parCoords) : 
        rep_id(parent_id), par_rot(parRot), par_coords(parCoords){}

DisjointSetForest::DisjointSetForest(int numNodes) : num_clusters(numNodes) {
    for (int i = 0; i < numNodes; i++) {
        nodes.push_back(DSFNode(i));
        MatrixXi coords(2, 1);
        coords << 0, 0;
        piece_coord_map[i] = coords;
    }
}

int DisjointSetForest::get_num_clusters() {
    return num_clusters;
}

Matrix2i DisjointSetForest::rot_mat(int r) {
    if (r == 0) {
        return Matrix2i::Identity();
    } else if (r == 1) {
        Matrix2i result;
        result << 0, -1, 1, 0;
        return result;
    } else {
        return -rot_mat(r - 2);
    }
}

SearchResult DisjointSetForest::find(int i) {
    auto pointer = &(nodes[i]);
    return find_node(pointer);
}

SearchResult DisjointSetForest::find_node(DSFNode *node) {
    if (node->get_parent_id() != node->get_id()) {
        SearchResult search_res = find(node->get_parent_id());
        if (node->get_parent_id() != search_res.rep_id) {
            node->set_parent_id(search_res.rep_id);
            int new_local_rot = py_mod(node->get_local_rot() + search_res.par_rot, 4);
            node->set_local_rot(new_local_rot);

            Vector2i shift = rot_mat(search_res.par_rot) * node->get_local_coords();
            Vector2i new_loc_coords = search_res.par_coords + shift;
            node->set_local_coords(new_loc_coords);
        }
    }
    return SearchResult(node->get_parent_id(), node->get_local_rot(), node->get_local_coords());
}

Vector2i compute_offset(int piece_small_switch) {
    Vector2i result;
    if (piece_small_switch == 0) {
        result << 1, 0;
    } else if (piece_small_switch == 1) {
        result << 0, 1;
    } else if (piece_small_switch == 2) {
        result << -1, 0;
    } else {
        result << 0, -1;
    }
    return result;
}

bool check_if_disjoint(const MatrixXi& coords_first, const MatrixXi& coords_second) {
    MatrixXi coords_first_t = coords_first.transpose();
    MatrixXi coords_second_t = coords_second.transpose();
    for (int i = 0; i < coords_first_t.rows(); i++) {
        for (int j = 0; j < coords_second_t.rows(); j++) {
            auto row1 = coords_first_t.row(i);
            auto row2 = coords_second_t.row(j);
            if (row1(0) == row2(0) && row1(1) == row2(1)) {
                return false;
            }
        }
    }
    return true;
}

int DisjointSetForest::forest_union(int i, int j, int edge_num_i, int edge_num_j) {

    auto i_result = find(i);
    auto j_result = find(j);

    if (i_result.rep_id == j_result.rep_id) {
        return -1;
    }

    auto clust_big = &nodes[j_result.rep_id];
	auto clust_small = &nodes[i_result.rep_id];
	auto piece_big = &nodes[j];
	auto piece_small = &nodes[i];
	int piece_rot_big = edge_num_j;
	int piece_rot_small = edge_num_i;
	bool big_is_left = false;

    if (nodes[i_result.rep_id].get_cluster_size() >= nodes[j_result.rep_id].get_cluster_size()) {
        clust_big = &nodes[i_result.rep_id];
		clust_small = &nodes[j_result.rep_id];
		piece_big = &nodes[i];
		piece_small = &nodes[j];
		piece_rot_big = edge_num_i;
		piece_rot_small = edge_num_j;
		big_is_left = true;
    }

    int expr = (piece_rot_small - piece_small->get_local_rot()) - (piece_rot_big - piece_big->get_local_rot());
    int small_clust_rot = py_mod(expr, 4);

    int val1 = -piece_rot_big + piece_big->get_local_rot();
    val1 = py_mod(val1, 4);
    int val2 = -piece_rot_big + 2 + piece_big->get_local_rot();
    val2 = py_mod(val2, 4);
    int piece_small_switch = big_is_left ? val1 : val2;

    Vector2i offset = compute_offset(piece_small_switch);

    Matrix2i small_rot_mat = rot_mat(small_clust_rot);

    Vector2i product = -(small_rot_mat * piece_small->get_local_coords());
    Vector2i small_clust_trans = piece_big->get_local_coords() + offset + product;

    MatrixXi coords_small = piece_coord_map[clust_small->get_piece_index()];

    MatrixXi mult_res = small_rot_mat * coords_small;
    MatrixXi new_coords_small = mult_res.colwise() + small_clust_trans;

    MatrixXi coords_big = piece_coord_map[clust_big->get_piece_index()];
   
    // Check that there will be no overlaps after gluing
    bool is_disjoint = check_if_disjoint(new_coords_small, coords_big);

    if (!is_disjoint) {
        return -1;
    }

    // Small cluster rot initially located in (0, 0), so new coords of it are equal to small_clust_trans
    clust_small->set_local_coords(small_clust_trans);
    clust_small->set_local_rot(small_clust_rot);
    clust_small->set_parent_id(clust_big->get_id());

    clust_big->append_to_cluster_size(clust_small->get_cluster_size());

    MatrixXi new_coords_big(coords_big.rows(), coords_big.cols() + new_coords_small.cols());
    new_coords_big << coords_big, new_coords_small;
    piece_coord_map[clust_big->get_piece_index()] = new_coords_big;
    
    piece_coord_map.erase(clust_small->get_piece_index());
    num_clusters -= 1;

    return clust_big->get_piece_index();
}

Mat DisjointSetForest::reconstruct(int cluster_index, const CutImage& pieces) {
    auto rep = nodes[find(cluster_index).rep_id];
    auto coords = piece_coord_map[rep.get_piece_index()];

    int min_x = coords.row(0).minCoeff();
    int min_y = coords.row(1).minCoeff();
    int max_x = coords.row(0).maxCoeff();
    int max_y = coords.row(1).maxCoeff();

    int P = pieces.get_square_size();
    int H = (max_y - min_y + 1) * P;
    int W = (max_x - min_x + 1) * P;

    auto type = pieces.get(0).get_rotated_pix(0).type();
    Mat img = Mat::zeros(H, W, type);
    
    for (const auto& node : nodes) {
        int ind = node.get_piece_index();
        auto node_rep = nodes[find(ind).rep_id];
        if (node_rep.get_id() != rep.get_id()) {
            continue;
        }
        auto sq = pieces.get(ind);
        auto pixels = sq.get_rotated_pix(node.get_local_rot());

        int x = node.get_local_coords()(0, 0) - min_x;
        int y = node.get_local_coords()(1, 0) - min_y;

        auto y_start = H - (y + 1) * P;
        auto y_size = P;

        auto x_start = x * P;
        auto x_size = P;

        auto rect = cv::Rect(x_start, y_start, x_size, y_size);
        pixels.copyTo(img(rect));
    }
    return img;
}

void DisjointSetForest::collapse() {
    for (auto& node : nodes) {
        find(node.get_piece_index());
    }
}

TrimArray DisjointSetForest::get_orig_trim_array() {
    MatrixXi coords = piece_coord_map[find(0).rep_id];

    int min_x = coords.row(0).minCoeff();
    int min_y = coords.row(1).minCoeff();
    int max_x = coords.row(0).maxCoeff();
    int max_y = coords.row(1).maxCoeff();
    
    TrimArray result(max_y - min_y + 1, max_x - min_x + 1);
    for (const auto& node : nodes) {
        int x = node.get_local_coords()(0, 0) - min_x;
        int y = node.get_local_coords()(1, 0) - min_y;
        result.set_index(y, x, node.get_piece_index());
        result.set_rot(y, x, node.get_local_rot());
    }

    return result;
}

Mat DisjointSetForest::reconstruct_trim(const TrimArray& index_grid, const CutImage& pieces) {
    auto type = pieces.get(0).get_rotated_pix(0).type();
    int H = index_grid.get_num_rows();
    int W = index_grid.get_num_columns();
    int P = pieces.get_square_size();
    Mat result = Mat::zeros(H * P, W * P, type);
    if (result.cols != 600 || result.rows != 500) {
        int a = 1;
    }
    for (int x = 0; x < W; x++) {
        for (int y = 0; y < H; y++) {
            if (index_grid.get_index(y, x) < 0) {
                continue;
            }

            ImagePiece sq = pieces.get(index_grid.get_index(y, x));

            Mat pixels = sq.get_rotated_pix(index_grid.get_rot(y, x));

            int x_init, y_init;
            y_init = H * P - (y + 1) * P;
            x_init = x * P;
            cv::Rect roi(x_init, y_init, P, P);
            pixels.copyTo(result(roi));
        }
    }
    return result;
}

std::tuple<int, int, int> get_best_frame_loc(const TrimArray& index_grid, int W, int H) {
    int H_prime = index_grid.get_num_rows();
    int W_prime = index_grid.get_num_columns();

    int best_x = -1;
    int best_y = -1;
    int best_count = -1;

    for (int x = 0; x < W_prime - W + 1; x++) {
        for (int y = 0; y < H_prime - H + 1; y++) {
            int sum = index_grid.get_occupied_count(y, x, H, W);
            if (sum > best_count) {
                best_x = x;
                best_y = y;
                best_count = sum;
            }
        }
    }
    return std::tie(best_x, best_y, best_count);
}

TrimArray DisjointSetForest::trim(const TrimArray& index_grid, int W, int H) {
    int x_0, y_0, best_count_0, x_1, y_1, best_count_1;

    auto best_frame_0 = get_best_frame_loc(index_grid, W, H);
    auto best_frame_1 = get_best_frame_loc(index_grid, H, W);

    x_0 = std::get<0>(best_frame_0);
    y_0 = std::get<1>(best_frame_0);
    best_count_0 = std::get<2>(best_frame_0);

    x_1 = std::get<0>(best_frame_1);
    y_1 = std::get<1>(best_frame_1);
    best_count_1 = std::get<2>(best_frame_1);

    int frame_x = x_0;
    int frame_y = y_0;
    int frame_w = W;
    int frame_h = H;

    if (best_count_0 < best_count_1) {
        frame_x = x_1;
        frame_y = y_1;
        frame_w = H;
        frame_h = W;
    }

    TrimArray result(frame_h, frame_w);
    int H_prime = index_grid.get_num_rows();
    int W_prime = index_grid.get_num_columns();
    for (int x = 0; x < W_prime; x++) {
        for (int y = 0; y < H_prime; y++) {
            bool cond = (x < frame_x || x >= frame_x + frame_w || y < frame_y || y >= frame_y + frame_h) 
                && index_grid.get_index(y, x) > 0;
            if (cond) {
                result.add_extra_piece(index_grid.get_index(y, x));
            }
        }
    }

    for (int x = frame_x; x < frame_x + frame_w; x++) {
        for (int y = frame_y; y < frame_y + frame_h; y++) {
            int ind = index_grid.get_index(y, x);
            if (ind == -1) {
                continue;
            }
            result.set_index(y - frame_y, x - frame_x, index_grid.get_index(y, x));
            result.set_rot(y - frame_y, x - frame_x, index_grid.get_rot(y, x));
        }
    }

    return result;
}

vector<plane_coord> DisjointSetForest::get_top_tier_holes(const TrimArray& index_grid) {
    vector<plane_coord> result;
    int tier = 1;
    int H = index_grid.get_num_rows();
    int W = index_grid.get_num_columns();
    for (int x = 0; x < W; x++) {
        for (int y = 0; y < H; y++) {
            if (index_grid.get_index(y, x) >= 0) {
                continue;
            }
            int counter = 0;
            if (x - 1 >= 0 && index_grid.get_index(y, x - 1) >= 0) {
                counter += 1;
            }
            if (y - 1 >= 0 && index_grid.get_index(y - 1, x) >= 0) {
                counter += 1;
            }
            if (x + 1 < W && index_grid.get_index(y, x + 1) >= 0) {
                counter += 1;
            }
            if (y + 1 < H && index_grid.get_index(y + 1, x) >= 0) {
                counter += 1;
            }

            if (counter == tier) {
                result.push_back(std::make_pair(x, y));
            } else if (counter > tier) {
                result.clear();
                result.push_back(std::make_pair(x, y));
                tier = counter;
            }
        }
    }
    return result;
}

TrimArray DisjointSetForest::fill(TrimArray& index_grid, const CutImage& img, bool ssd) {
    //std::cout << "Filling started with " << index_grid.get_extra_pieces_size() << " extra pieces" << std::endl;
    while (index_grid.get_extra_pieces_size() > 0) {
        vector<plane_coord> top_tier_holes = get_top_tier_holes(index_grid);
        // If there is no holes anymore, then we are out of remaining pieces
        if (top_tier_holes.size() == 0) {
            break;
        }

        int best_extra = -1;
        int best_rot = -1;
        double best_dist = MAXFLOAT;
        int best_hole_x = -1;
        int best_hole_y = -1;

        for (const auto& [x, y] : top_tier_holes) {
            for (int i = 0; i < index_grid.get_extra_pieces_size(); i++) {
                for (int extra_rot = 0; extra_rot < 4; extra_rot++) {
                    double total_dist = 0;
                    for (int flag = 0; flag < 4; flag++) {
                        total_dist += get_edge_cost(x, y, index_grid, index_grid.get_extra_piece(i), extra_rot, flag, img, ssd);
                    }
                    if (total_dist < best_dist) {
                        best_dist = total_dist;
                        best_extra = index_grid.get_extra_piece(i);
                        best_rot = extra_rot;
                        best_hole_x = x;
                        best_hole_y = y;
                    }
                }
            }
        }

        index_grid.set_index(best_hole_y, best_hole_x, best_extra);
        index_grid.set_rot(best_hole_y, best_hole_x, best_rot);
        index_grid.remove_extra_piece(best_extra);
    }
    return index_grid;
}