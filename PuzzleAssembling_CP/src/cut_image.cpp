#include <vector>
#include <set>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <random>

#include <experimental/filesystem>
#include <fstream>

#include <Eigen/Core>

#include "image_piece.h"
#include "cut_image.h"
#include "utilities.h"


using Eigen::MatrixXi;
using cv::Mat;
using cv::Rect;
using std::pair;
using std::vector;
using std::set;
using std::make_pair;
using std::default_random_engine;

namespace fs = std::experimental::filesystem;

string metafile_name = "metainfo.txt";

SplitResult::SplitResult() {}

void SplitResult::add_image_piece(ImagePiece piece) {
    pieces.push_back(piece);
}

void SplitResult::add_true_connection(int index_i, int index_j, int rot_i, int rot_j) {
    double dist = 0;
    true_connections.push_back(std::tie(dist, index_i, index_j, rot_i, rot_j));
}

vector<ImagePiece> SplitResult::get_image_pieces() const {
    return pieces;
}

vector<edge_def> SplitResult::get_true_connections() const {
    return true_connections;
}

SplitResult CutImage::split_picture(const Mat& picture, int rect_height, int rect_width, bool type2, int start_with) {
	int num_chunks_x = picture.cols / rect_width;
	int num_chunks_y = picture.rows / rect_height;

    MatrixXi indexes(num_chunks_y, num_chunks_x);
    MatrixXi rotations(num_chunks_y, num_chunks_x);

    true_indexes = MatrixXi::Zero(num_chunks_y, num_chunks_x);
    true_rotations = MatrixXi::Zero(num_chunks_y, num_chunks_x);

	SplitResult result;
    int count = start_with;
	for (int j = 0; j < num_chunks_y; j++) {
		for (int i = 0; i < num_chunks_x; i++) {
			Rect rect(i * rect_width, j * rect_height, rect_width, rect_height);
            Mat pixels;
			picture(rect).copyTo(pixels);

            // if we use type 2 task, we need to rotate every piece randomly.
            int rotation_num = 0;
            if (type2) {
                rotation_num = py_mod(rand(), 4);
                for (int i = 0; i < rotation_num; i++) {
                    cv::rotate(pixels, pixels, cv::ROTATE_90_COUNTERCLOCKWISE);
                }
            }

            indexes(j, i) = count;
            rotations(j, i) = rotation_num;

            true_indexes(j, i) = count;
            true_rotations(j, i) = py_mod(-rotation_num, 4);

            ImagePiece piece(pixels, j, i, count, rotation_num);
			result.add_image_piece(piece);
            count++;
		}
	}

    for (int i = 0; i < num_chunks_y; i++) {
        for (int j = 0; j < num_chunks_x; j++) {
            if (i > 0) {
                // add top neighbour
                int rot1 = py_mod(3 - rotations(i, j), 4);
                int rot2 = py_mod(3 - rotations(i-1, j), 4);
                int ind1 = indexes(i, j);
                int ind2 = indexes(i-1, j);
                result.add_true_connection(ind1, ind2, rot1, rot2);
            }
            if (j > 0) {
                // add left neighbour
                int rot1 = py_mod(2 - rotations(i, j), 4);
                int rot2 = py_mod(2 - rotations(i, j-1), 4);
                int ind1 = indexes(i, j);
                int ind2 = indexes(i, j-1);
                result.add_true_connection(ind1, ind2, rot1, rot2);
            }
            if (i < num_chunks_y - 1) {
                // add bottom neighbour
                int rot1 = py_mod(1 - rotations(i, j), 4);
                int rot2 = py_mod(1 - rotations(i+1, j), 4);
                int ind1 = indexes(i, j);
                int ind2 = indexes(i+1, j);
                result.add_true_connection(ind1, ind2, rot1, rot2);
            }
            if (j < num_chunks_x - 1) {
                // add right neighbour
                int rot1 = py_mod(-rotations(i, j), 4);
                int rot2 = py_mod(-rotations(i, j+1), 4);
                int ind1 = indexes(i, j);
                int ind2 = indexes(i, j+1);
                result.add_true_connection(ind1, ind2, rot1, rot2);
            }
        }
    }

	return result;
}

int gcd (int a, int b) {
    while (b) {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}

int compute_square_size(const set<int> sizes) {
    vector<int> sizes_vec(sizes.begin(), sizes.end());
    int result = sizes_vec[0];
    for (int i = 1; i < sizes_vec.size(); i++) {
        result = gcd(result, sizes_vec[i]);
    }
    return result;
}

bool validate_matrix(vector<vector<Mat>> matrix) {
    int size = -1;
    for (int i = 0; i < matrix.size(); i++) {
        if (size == -1) {
            size = matrix[i].size();
        } else {
            if (size != matrix[i].size()) {
                return false;
            }
        }
    }
    return true;
}

void validate_split(const vector<ImagePiece>& pieces);

CutImage::CutImage(Mat picture, int rectHeight, int rectWidth, bool type2) {

    image = picture;
    width_in_squares = image.cols / rectWidth;
	height_in_squares = image.rows / rectHeight;
    rect_width = rectWidth;
    rect_height = rectHeight;

    Rect border(0, 0, width_in_squares * rect_width, height_in_squares * rect_height);
    reduce_image = picture(border);

    SplitResult res = split_picture(image, rect_height, rect_width, type2);
    true_connections = res.get_true_connections();
    pieces = res.get_image_pieces();

    set<int> sizes;
    for (const auto& p : pieces) {
        sizes.insert(p.get_rotated_pix(0).rows);
        sizes.insert(p.get_rotated_pix(0).cols);
    }
    square_size = compute_square_size(sizes);
}

CutImage::CutImage(vector<Mat> images, int heightInSquares, int widthInSquares, int rectHeight, int rectWidth) {
    set<int> sizes;
    for (const auto& piece : images) {
        sizes.insert(piece.rows);
        sizes.insert(piece.cols);
    }
    square_size = compute_square_size(sizes);
    width_in_squares = widthInSquares * rectWidth / square_size;
    height_in_squares = heightInSquares * rectHeight / square_size;
    rect_width = rectWidth;
    rect_height = rectHeight;
    pieces.clear();

    int parent_piece_id = 0;

    for (const auto& img : images) {
        SplitResult res = split_picture(img, square_size, square_size, true, pieces.size());
        vector<ImagePiece> result = res.get_image_pieces();
        for (auto& piece : result) {
            pieces.push_back(piece);
            pieces[pieces.size() - 1].set_parent_piece_id(parent_piece_id);
        }
        vector<edge_def> conns = res.get_true_connections();
        for (const auto& conn : conns) {
            true_connections.push_back(conn);
        }
        parent_piece_id++;
    }
}

pair<int, int> CutImage::get_size() const {
    return make_pair(height_in_squares, width_in_squares);
}

int CutImage::get_pieces_count() const {
    return pieces.size();
}

int CutImage::get_square_size() const {
    return square_size;
}

void CutImage::write_to_directory(const string& directory) const {
    fs::remove_all(directory);
    fs::create_directories(directory);
    for (const auto& piece : pieces) {
        piece.write_pieces_to_dir(directory);
    }
    std::ofstream meta(directory + metafile_name);
    meta << height_in_squares << " " << width_in_squares << " " << rect_height << " " << rect_width << std::endl;
}

vector<edge_def> CutImage::get_true_connections() const {
    return true_connections;
}

void CutImage::shuffle() {
	random_shuffle(pieces.begin(), pieces.end());
}

void CutImage::compute_means_and_cov_invs(bool type2) {
    for (auto& piece : pieces) {
        piece.compute_mean_and_cov_inv(type2);
    }
}

Mat CutImage::to_mat() {
    Mat dest;
	vector<Mat> local_dests;
	for (int i = 0; i < height_in_squares; i++) {
        vector<Mat> mat_pieces;
        for (int j = 0; j < width_in_squares; j++) {
            mat_pieces.push_back(pieces[i * height_in_squares + j].get_rotated_pix(0));
        }
		Mat local_dest;
		hconcat(mat_pieces, local_dest);
		local_dests.push_back(local_dest);
	}
    vconcat(local_dests, dest);
    return dest;
}

ImagePiece& CutImage::at(int i, int j) {
    
    if (i >= height_in_squares || i < 0) {
        throw std::invalid_argument("First index is out of range");
    } else if (j >= width_in_squares || j < 0) {
        throw std::invalid_argument("Second index is out of range");
    } else {
        return pieces[i * height_in_squares + j];
    }
}

ImagePiece CutImage::get(int i, int j) const {

    if (i >= height_in_squares || i < 0) {
        throw std::invalid_argument("First index is out of range");
    } else if (j >= width_in_squares || j < 0) {
        throw std::invalid_argument("Second index is out of range");
    } else {
        return pieces[i * height_in_squares + j];
    }
}

ImagePiece& CutImage::at(int i) {
    return pieces[i];
}

int CutImage::get_parent_piece_id(int piece_id) const {
    return pieces.at(piece_id).parent_piece_id;
}

ImagePiece CutImage::get(int i) const {
    if (i < 0 || i > get_pieces_count()) {
        int a = 1;
    }
    return pieces[i];
}

CutImage read_from_directory(const string& directory) {

    if (!fs::exists(directory + metafile_name)) {
        throw std::invalid_argument("Incorrect directory structure. Metadata file is not found.");
    }

    std::ifstream meta(directory + metafile_name);

    int height_in_squares, width_in_squares, rect_height, rect_width;
    meta >> height_in_squares >> width_in_squares >> rect_height >> rect_width;

    vector<Mat> pictures;
    for (const auto& entity : fs::directory_iterator(directory)) {
        fs::path p = entity.path();
        string filename = p.filename().u8string();
        if (p.filename().u8string() + p.extension().u8string() == metafile_name) {
            continue;
        }
        Mat pixels = cv::imread(p.u8string(), cv::IMREAD_COLOR);
        pictures.push_back(pixels);
    }
    return CutImage(pictures, height_in_squares, width_in_squares, rect_height, rect_width);
}