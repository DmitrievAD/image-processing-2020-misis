#include <vector>
#include <string>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "image_piece.h"
#include "utilities.h"

using std::vector;
using std::string;
using cv::Mat;

using Eigen::MatrixXd;
using Eigen::Matrix3d;
using Eigen::Vector3d;


MatrixXd get_dummy_grad() {
    MatrixXd dummy_grad(9, 3);
    dummy_grad <<  0,  0,  0, 
                   1,  1,  1, 
                  -1, -1, -1, 
                   0,  0,  1, 
                   0,  1,  0, 
                   1,  0,  0, 
                  -1,  0,  0, 
                   0, -1,  0, 
                   0,  0, -1;
    return dummy_grad;
}

ImagePiece::ImagePiece(const Mat& piece_data, int originalXPos, int originalYPos, int count, int rotationNum) 
                    : pixels(piece_data), idx(count), rotation_num(rotationNum), 
                      original_x_pos(originalXPos), original_y_pos(originalYPos), dummy_grad(get_dummy_grad()) {}

ImagePiece::ImagePiece(const ImagePiece& other_piece) : pixels(other_piece.get_rotated_pix(0)), idx(other_piece.get_idx()), 
                                                        rotation_num(other_piece.get_rotation_num()), original_x_pos(other_piece.get_original_x_pos()),
                                                        original_y_pos(other_piece.get_original_y_pos()), dummy_grad(get_dummy_grad()) {
    means = other_piece.get_means();
    covar_inv = other_piece.get_covar_inv();
}

ImagePiece& ImagePiece::operator=(const ImagePiece& other_piece) {
    this -> ~ImagePiece();
    new (this) ImagePiece(other_piece);
    return *this;
}

Mat ImagePiece::get_rotated_pix(int rot) const {
    Mat copy;
    pixels.copyTo(copy);
    for (int i = 0; i < rot; i++) {
        cv::rotate(copy, copy, cv::ROTATE_90_COUNTERCLOCKWISE);
    }
    return copy;
}

MatrixXd col_to_matrix(const Mat& col) {
    MatrixXd m1(col.rows, 3);
    for (int i = 0; i < col.rows; i++) {
        auto scalar = col.at<cv::Vec3b>(i, 0);
        m1(i, 0) = (int)scalar[1]; // red
        m1(i, 1) = (int)scalar[1]; // green
        m1(i, 2) = (int)scalar[2]; // blue
    }
    return m1;
}

MatrixXd compute_border_grad(Mat pixels) {
    auto rect_left = cv::Rect(pixels.cols - 1, 0, 1, pixels.rows);
    auto rect_right = cv::Rect(pixels.cols - 2, 0, 1, pixels.rows);
    MatrixXd rect_left_m = col_to_matrix(pixels(rect_left));
    MatrixXd rect_right_m = col_to_matrix(pixels(rect_right));
    return rect_left_m - rect_right_m;
}

MatrixXd covariance_matrix(MatrixXd source) { 
    MatrixXd centred = source.rowwise() - source.colwise().mean();
    double fact = source.rows() - 1;
    MatrixXd cov = (centred.transpose() * centred) / fact;
    return cov;
}

void ImagePiece::compute_mean_and_cov_inv(bool type2) {
    means.clear();
    covar_inv.clear();
    
    for (int rot = 0; rot < 4; rot++) {

        auto pixels_copy = get_rotated_pix(rot);

        MatrixXd GL = compute_border_grad(pixels_copy);

        Vector3d mu = GL.colwise().mean();

        MatrixXd GL_plus_dummy(GL.rows() + 9, 3);
        GL_plus_dummy << GL, dummy_grad;

        Matrix3d cov_matrix = covariance_matrix(GL_plus_dummy);
        Matrix3d cov_inv = cov_matrix.inverse();

        means.push_back(mu);
		covar_inv.push_back(cov_inv);
    }
}

void ImagePiece::write_pieces_to_dir(const string& directory) const {
    cv::imwrite(directory + std::to_string(idx) + ".png", pixels);
}

Vector3d ImagePiece::get_right_mean(int rot) const {
    return means[rot];
}

Vector3d ImagePiece::get_left_mean(int rot) const {
    return means[py_mod(rot + 2, 4)];
}

Matrix3d ImagePiece::get_right_cov_inv(int rot) const{
    return covar_inv[rot];
}

Matrix3d ImagePiece::get_left_cov_inv(int rot) const {
    return covar_inv[py_mod(rot + 2, 4)];
}

std::pair<int, int> ImagePiece::get_size() const {
    return std::make_pair(pixels.rows, pixels.cols);
}

int ImagePiece::get_idx() const {
    return idx;
}

int ImagePiece::get_rotation_num() const {
    return rotation_num;
}

int ImagePiece::get_original_x_pos() const {
    return original_x_pos;
}

int ImagePiece::get_original_y_pos() const {
    return original_y_pos;
}

vector<Vector3d> ImagePiece::get_means() const {
    return means;
}

vector<Matrix3d> ImagePiece::get_covar_inv() const {
    return covar_inv;
}

void swap(ImagePiece& i1, ImagePiece& i2) {
    auto i1_copy = ImagePiece(i1);
    i1 = ImagePiece(i2);
    i2 = i1_copy;
}

void ImagePiece::set_parent_piece_id(int new_parent_piece_id) {
    parent_piece_id = new_parent_piece_id;
}

int ImagePiece::get_parent_piece_id() const {
    return parent_piece_id;
}