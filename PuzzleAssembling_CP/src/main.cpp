#include <iostream>
#include <cmath>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <random>
#include <experimental/filesystem>

#include "cut_image.h"
#include "picture_assembler.h"
#include "edge_scores.h"

using namespace std;
using namespace cv;

namespace fs = std::experimental::filesystem;


bool type2 = true;
bool ssd = false;
bool div_by_sec_small = true;

// /media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/

void prepare_results_folder(string path) {
	std::experimental::filesystem::remove_all(path);
	std::experimental::filesystem::create_directories(path);
}

double reassemble_picture(string pic_name, string result_folder, string tmp_folder, int pix_height, int pix_width) {
	prepare_results_folder(result_folder);

	Mat image = imread(pic_name, IMREAD_COLOR);

	cv::imwrite(result_folder + "source.png", image);

	cv::cvtColor(image, image, cv::COLOR_BGR2RGB);

	CutImage cut_img(image, pix_height, pix_width, type2);
	cut_img.shuffle();
	cut_img.compute_means_and_cov_invs(type2);
	//cut_img.write_to_directory(tmp_folder);

	//cv::imshow("shuffled", cut_img.to_mat());
	//cv::imwrite(result_folder + "shuffled.png", cut_img.to_mat());

	//CutImage img = read_from_directory(tmp_folder);
    //img.compute_means_and_cov_invs(type2);

	std::pair<Mat, double> res = assemble_picture(cut_img, 32, 250, type2, ssd, div_by_sec_small, result_folder);
	Mat reassembled = std::get<0>(res);
	double score = std::get<1>(res);
	return score;
}

double compute_neigh_metric(string pic_name, int pix_height, int pix_width) {
	Mat image = imread(pic_name, IMREAD_COLOR);
	CutImage cut_img(image, pix_height, pix_width, type2);
	cut_img.compute_means_and_cov_invs(type2);

	EdgeDistancesContainer distances = compute_edge_dists(cut_img, 32, 250, type2, ssd, div_by_sec_small);

	//double score = compute_neighbour_metric(cut_img, distances);
}

void compute_scores() {
	double metric_sum = 0;
	int pix_height = 30;
	int pix_width = 30;

	std::cout << "Using ssd metric" << std::endl;

	for (int name = 1; name <= 20; name++) {

		string IMAGE_FOLDER = "images/";
		string IMAGE_NAME = to_string(name);
		string IMAGE_EXT = ".png";
		string RESULT_FOLDER = (type2 ? "results/type_2/" : "results/type_1/") + IMAGE_NAME + "/";
		string TMP_FOLDER = "tmp/";

		std::cout << "Saving result into " << RESULT_FOLDER << std::endl;

		double metric = reassemble_picture(IMAGE_FOLDER + IMAGE_NAME + IMAGE_EXT, RESULT_FOLDER, TMP_FOLDER, pix_height, pix_width);

		//double metric = compute_neigh_metric(IMAGE_FOLDER + IMAGE_NAME + IMAGE_EXT, pix_height, pix_width);
		std::cout << "Assembly completed! Neighbour metric value is " << metric << std::endl;
		metric_sum += metric;
	}
	std::cout << "Overall metric value is " << metric_sum / 20 << std::endl;

	std::cout << "Using mgc metric" << std::endl;

	metric_sum = 0;

	for (int name = 1; name <= 20; name++) {

		string IMAGE_FOLDER = "images/";
		string IMAGE_NAME = to_string(name);
		string IMAGE_EXT = ".png";
		string RESULT_FOLDER = (type2 ? "results/type_2/" : "results/type_1/") + IMAGE_NAME + "/";
		string TMP_FOLDER = "tmp/";

		std::cout << "Saving result into " << RESULT_FOLDER << std::endl;

		double metric = reassemble_picture(IMAGE_FOLDER + IMAGE_NAME + IMAGE_EXT, RESULT_FOLDER, TMP_FOLDER, pix_height, pix_width);

		//double metric = compute_neigh_metric(IMAGE_FOLDER + IMAGE_NAME + IMAGE_EXT, pix_height, pix_width);
		std::cout << "Assembly completed! Neighbour metric value is " << metric << std::endl;
		metric_sum += metric;
	}
	std::cout << "Overall metric value is " << metric_sum / 20 << std::endl;

	//int pix_height = 40;
	//int pix_width = 40;

	string IMAGE_FOLDER = "images/";
	string IMAGE_NAME = "elephant";
	string IMAGE_EXT = ".jpg";
	string RESULT_FOLDER = (type2 ? "results/type_2/" : "results/type_1/") + IMAGE_NAME + "/";
	string TMP_FOLDER = "tmp/";

	//reassemble_picture(IMAGE_FOLDER + IMAGE_NAME + IMAGE_EXT, RESULT_FOLDER, TMP_FOLDER, pix_height, pix_width);
	
	double metric = reassemble_picture(IMAGE_FOLDER + IMAGE_NAME + IMAGE_EXT, RESULT_FOLDER, TMP_FOLDER, pix_height, pix_width);
	std::cout << "Metric on elephant.jpeg picture is " << metric << std::endl;
}

// To compute scores call compute_scores() in the main func. 
// Computations may take a long time.
// You can configure computations by setting pix_height and pix_width
int main() {
	string p, im_name;
	std::cout << "Enter path to patch folder: ";
	std::cin >> p;
	std::cout << "Enter the name of the image: ";
	std::cin >> im_name;

	CutImage img = read_from_directory(p);
	std::cout << "Patches red succesfully!" << std::endl;
	std::cout << "Square size is: " << img.get_square_size() << std::endl;

	img.compute_means_and_cov_invs(type2);

	string RESULT_FOLDER = (type2 ? "results/type_2/" : "results/type_1/") + im_name + "/";
	prepare_results_folder(RESULT_FOLDER);
	std::cout << "Writing results into folder: " << RESULT_FOLDER << std::endl;
	
	std::pair<Mat, double> res = assemble_picture(img, 32, 250, type2, ssd, div_by_sec_small, RESULT_FOLDER);

	return 0;
}