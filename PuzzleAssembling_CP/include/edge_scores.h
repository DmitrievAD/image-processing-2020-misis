#pragma once

#include <vector>
#include <tuple>
#include <opencv2/core.hpp>
#include <map>
#include <Eigen/Core>

#include "cut_image.h"
#include "trim_array.h"

using std::vector;
using std::tuple;
using Eigen::Vector3d;
using Eigen::Matrix3d;
using cv::Mat;

using edge_def = tuple<double, int, int, int, int>;

class EdgeDistancesContainer {
    public:
        EdgeDistancesContainer();

        void add_result(int first_ind, int first_rot, int second_ind, int second_rot, double dist);

        void divide_distances_by_second_smalest(int target_ind, int target_rot);

        int size();

        bool is_empty();

        void heapify();

        edge_def heappop();

    private:
        vector<int> first_indexes;
        vector<int> first_rotations;
        vector<int> second_indexes;
        vector<int> second_rotations;
        vector<double> distances;
        vector<edge_def> heap_form;
};

EdgeDistancesContainer compute_edge_dists(const CutImage& image, int num_buckets, 
                                          int min_neighbours_num, bool type2, 
                                          bool ssd, bool div_by_sec_smalest);

double get_compat(const Vector3d& left_mean, const Matrix3d& left_covar_inv, 
                  const Vector3d& right_mean, const Matrix3d& right_covar_inv, 
                  const Mat& pix1, const Mat& pix2);

double get_edge_cost(int x, int y, const TrimArray& index_grid, int piece_ind, int piece_rot, int neigh_flag, const CutImage& img, bool ssd);