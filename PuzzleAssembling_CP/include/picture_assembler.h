#pragma once

#include <opencv2/core.hpp>
#include <string>

#include "cut_image.h"
#include "edge_scores.h"

using std::string;
using cv::Mat;

double compute_neighbour_metric(const CutImage& img, EdgeDistancesContainer cont);

std::pair<Mat, double> assemble_picture(const CutImage& img, int num_buckets, int min_neigh, bool type2, bool ssd, bool div_by_sec_small, const string& save_dir);