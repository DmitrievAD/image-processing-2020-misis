#pragma once

#include <vector>
#include <Eigen/Core>

using Eigen::Vector2i;


class DSFNode {
    public:
        DSFNode(int i);

        DSFNode& operator=(const DSFNode& right);

        void set_id(int new_id);
        void set_parent_id(int new_parent_id);
        void append_to_cluster_size(int cluster_size_increasing);
        void set_local_rot(int new_local_rot);
        void set_local_coords(Vector2i new_local_coords);

        int get_id() const;
        int get_parent_id() const;
        int get_piece_index() const;
        int get_local_rot() const;
        Vector2i get_local_coords() const;
        int get_cluster_size() const;

        static int get_next_id();

    private:
        int parent_id;
		int local_rot;
        int cluster_size;
        int piece_index;
        Vector2i local_coords;
        int id;
};