#pragma once

#include <vector>
#include <opencv2/core.hpp>
#include <tuple>

#include "image_piece.h"
#include "trim_array.h"

using std::vector;
using std::pair;
using std::tuple;
using cv::Mat;

using edge_def = tuple<double, int, int, int, int>;

class SplitResult {
    public:
        SplitResult();

        void add_image_piece(ImagePiece piece);

        void add_true_connection(int piece_num_i, int piece_num_j, int rot_i, int rot_j);

        vector<ImagePiece> get_image_pieces() const;

        vector<edge_def> get_true_connections() const;

    private:
        vector<ImagePiece> pieces;
        vector<edge_def> true_connections;
};

class CutImage {
    public:

        CutImage(Mat picture, int rect_height, int rect_width, bool type2);

        CutImage(vector<Mat> images, int heightInSquares, int widthInSquares, int rect_height, int rect_width);

        // returns the pair object containing height and width in pieces
        pair<int, int> get_size() const;

        int get_pieces_count() const;

        int get_square_size() const;

        vector<edge_def> get_true_connections() const;

        void shuffle();

        void compute_means_and_cov_invs(bool type2);

        void write_to_directory(const string& directory) const;

        Mat to_mat();

        ImagePiece& at(int i, int j);

        ImagePiece get(int i, int j) const;

        ImagePiece& at(int i);

        int get_parent_piece_id(int piece_id) const;

        ImagePiece get(int i) const;

        double compute_direct_metric(const TrimArray& result) const;

        MatrixXi true_indexes;
        MatrixXi true_rotations;

    private:

        SplitResult split_picture(const Mat& picture, int rect_height, int rect_width, bool type2, int start_with = 0);

        vector<ImagePiece> pieces;
        vector<edge_def> true_connections;
        Mat image;
        Mat reduce_image;
        int square_size;
        int rect_height;
        int rect_width;
        int height_in_squares;
        int width_in_squares;
};


CutImage read_from_directory(const string& directory);