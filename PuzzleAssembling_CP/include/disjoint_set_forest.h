#pragma once

#include <vector>
#include <tuple>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <map>
#include <Eigen/Core>

#include "dsf_node.h"
#include "trim_array.h"
#include "cut_image.h"

using Eigen::MatrixXi;
using Eigen::Matrix2i;

using std::vector;
using std::tuple;
using std::map;
using cv::Mat;

using plane_coord = std::pair<int, int>;

class SearchResult {

    public:
        SearchResult(int parent_id, int parRot, MatrixXi parCoords);

        int rep_id;
        const int par_rot;
        const MatrixXi par_coords;
};

class DisjointSetForest {

    public:
        DisjointSetForest(int numNodes);

        map<int, MatrixXi> piece_coord_map;

        Matrix2i rot_mat(int r);

        SearchResult find(int i);

        SearchResult find_node(DSFNode *node);

        int forest_union(int i, int j, int edge_num_i, int edge_num_j);

        Mat reconstruct(int cluster_index, const CutImage& pieces);

        Mat reconstruct_trim(const TrimArray& index_grid, const CutImage& pieces);

        TrimArray trim(const TrimArray& index_grid, int W, int H);

        void collapse();

        int get_num_clusters();

        TrimArray get_orig_trim_array();

        TrimArray fill(TrimArray& index_grid, const CutImage& img, bool ssd);

        vector<plane_coord> get_top_tier_holes(const TrimArray& index_grid) ;

    private:
        int num_clusters;
        vector<DSFNode> nodes;
};

bool check_if_disjoint(const MatrixXi& coords_first, const MatrixXi& coords_second);