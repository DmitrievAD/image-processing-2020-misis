#pragma once

#include <Eigen/Core>
#include <vector>

using Eigen::MatrixXi;
using std::vector;


class TrimArray {

    public:
        TrimArray(int rows, int cols);

        void set_index(int row, int col, int ind);
        void set_rot(int row, int col, int rot);
        void set_occupied(int row, int col, int occ);
        void add_extra_piece(int piece_ind);
        void remove_extra_piece(int index);

        int get_index(int row, int col) const;
        int get_rot(int row, int col) const;
        int get_occupied(int row, int col) const;
        int get_extra_piece(int ind) const;
        int get_num_rows() const;
        int get_num_columns() const;
        int get_extra_pieces_size() const;

        int get_occupied_count(int row, int col, int height, int width) const;

        MatrixXi index_grid;
        MatrixXi rot_grid;
        MatrixXi occupied_grid;
        vector<int> extra_pieces_list;

    private:
        void validate_get(int row, int col) const;
        void validate_slice(int row, int col, int height, int width) const;
};