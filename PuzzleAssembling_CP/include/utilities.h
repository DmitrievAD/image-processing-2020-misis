#pragma once

#include <tuple>
#include <opencv2/core.hpp>

using cv::Mat;
using std::tuple;


// This function is different from standart % operator.
// The implementation below guarantee that output is in range [0, b)
template <class T>
T py_mod(T a, T b) {
    if (a % b == 0) {
        return 0;
    } else if (a < 0) {
        T res = b + (a % b);
        return res;
    } else {
        T res = a % b;
        return res;
    }
}
