#pragma once

#include <vector>
#include <string>
#include <opencv2/core.hpp>
#include <Eigen/Core>

using std::vector;
using std::string;
using cv::Mat;

using Eigen::Matrix3d;
using Eigen::MatrixXd;
using Eigen::Vector3d;

class ImagePiece {
    public:
        ImagePiece(const Mat& piece_data, int originalXPos, int originalYPos, int count, int rotationNum);

        ImagePiece(const ImagePiece& other_piece);

        int get_idx() const;
        int get_rotation_num() const;
        int get_original_x_pos() const;
        int get_original_y_pos() const;
        vector<Vector3d> get_means() const;
        vector<Matrix3d> get_covar_inv() const;

        Mat get_rotated_pix(int rot) const;

        void compute_mean_and_cov_inv(bool type2);
        void write_pieces_to_dir(const string& directory) const;

        Vector3d get_left_mean(int rot) const;
        Vector3d get_right_mean(int rot) const;
        Matrix3d get_left_cov_inv(int rot) const;
        Matrix3d get_right_cov_inv(int rot) const;
        std::pair<int, int> get_size() const;

        void set_parent_piece_id(int new_parent_piece_id);
        int get_parent_piece_id() const;

        ImagePiece& operator=(const ImagePiece& other_piece);

        int parent_piece_id;

    private:
        const Mat pixels;
        const int idx;
        const int rotation_num;
        const int original_x_pos;
        const int original_y_pos;

        vector<Vector3d> means;
        vector<Matrix3d> covar_inv;

        const MatrixXd dummy_grad;
};

void swap(ImagePiece& i1, ImagePiece& i2);