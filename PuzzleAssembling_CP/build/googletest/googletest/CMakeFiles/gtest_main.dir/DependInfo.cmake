# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/googletest/googletest/src/gtest_main.cc" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/googletest/googletest/CMakeFiles/gtest_main.dir/src/gtest_main.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../googletest/googletest/include"
  "../googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/googletest/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
