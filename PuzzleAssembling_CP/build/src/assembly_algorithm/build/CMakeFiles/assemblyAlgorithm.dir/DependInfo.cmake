# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/edge_scores.cpp" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src/assembly_algorithm/build/CMakeFiles/assemblyAlgorithm.dir/edge_scores.cpp.o"
  "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/picture_assembler.cpp" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src/assembly_algorithm/build/CMakeFiles/assemblyAlgorithm.dir/picture_assembler.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src/assembly_algorithm/build/CMakeFiles/DSF.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
