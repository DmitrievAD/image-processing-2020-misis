# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP"

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build"

# Include any dependencies generated for this target.
include src/CMakeFiles/assemblyAlgorithm.dir/depend.make

# Include the progress variables for this target.
include src/CMakeFiles/assemblyAlgorithm.dir/progress.make

# Include the compile flags for this target's objects.
include src/CMakeFiles/assemblyAlgorithm.dir/flags.make

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o: src/CMakeFiles/assemblyAlgorithm.dir/flags.make
src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o: ../src/assembly_algorithm/edge_scores.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir="/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/CMakeFiles" --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && /usr/bin/g++-7  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o -c "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/edge_scores.cpp"

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.i"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && /usr/bin/g++-7 $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/edge_scores.cpp" > CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.i

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.s"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && /usr/bin/g++-7 $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/edge_scores.cpp" -o CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.s

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.requires:

.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.requires

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.provides: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.requires
	$(MAKE) -f src/CMakeFiles/assemblyAlgorithm.dir/build.make src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.provides.build
.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.provides

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.provides.build: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o


src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o: src/CMakeFiles/assemblyAlgorithm.dir/flags.make
src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o: ../src/assembly_algorithm/picture_assembler.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir="/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/CMakeFiles" --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && /usr/bin/g++-7  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o -c "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/picture_assembler.cpp"

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.i"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && /usr/bin/g++-7 $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/picture_assembler.cpp" > CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.i

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.s"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && /usr/bin/g++-7 $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src/assembly_algorithm/picture_assembler.cpp" -o CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.s

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.requires:

.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.requires

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.provides: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.requires
	$(MAKE) -f src/CMakeFiles/assemblyAlgorithm.dir/build.make src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.provides.build
.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.provides

src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.provides.build: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o


# Object files for target assemblyAlgorithm
assemblyAlgorithm_OBJECTS = \
"CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o" \
"CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o"

# External object files for target assemblyAlgorithm
assemblyAlgorithm_EXTERNAL_OBJECTS =

src/libassemblyAlgorithm.a: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o
src/libassemblyAlgorithm.a: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o
src/libassemblyAlgorithm.a: src/CMakeFiles/assemblyAlgorithm.dir/build.make
src/libassemblyAlgorithm.a: src/CMakeFiles/assemblyAlgorithm.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir="/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/CMakeFiles" --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX static library libassemblyAlgorithm.a"
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && $(CMAKE_COMMAND) -P CMakeFiles/assemblyAlgorithm.dir/cmake_clean_target.cmake
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/assemblyAlgorithm.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/CMakeFiles/assemblyAlgorithm.dir/build: src/libassemblyAlgorithm.a

.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/build

src/CMakeFiles/assemblyAlgorithm.dir/requires: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/edge_scores.cpp.o.requires
src/CMakeFiles/assemblyAlgorithm.dir/requires: src/CMakeFiles/assemblyAlgorithm.dir/assembly_algorithm/picture_assembler.cpp.o.requires

.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/requires

src/CMakeFiles/assemblyAlgorithm.dir/clean:
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" && $(CMAKE_COMMAND) -P CMakeFiles/assemblyAlgorithm.dir/cmake_clean.cmake
.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/clean

src/CMakeFiles/assemblyAlgorithm.dir/depend:
	cd "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build" && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/src" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src" "/media/anton/D/Projects/Study/Image Processing/PuzzleAssembling_CP/build/src/CMakeFiles/assemblyAlgorithm.dir/DependInfo.cmake" --color=$(COLOR)
.PHONY : src/CMakeFiles/assemblyAlgorithm.dir/depend

