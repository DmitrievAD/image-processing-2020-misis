# 1. Гамма-коррекция серой градиентной заливки
автор: Дмитриев А.Д.\
url: https://gitlab.com/DmitrievAD
### Задание
1. Часть 1
    1. Нарисовать прямоугольник размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
    2. Под ним  изображение этого градиента после гамма-коррекции с коэффициентом из интервала 2.2-2.4
2. Часть 2
    1. Нарисовать прямоугольник размером 768х60 пикселя со ступенчатым изменение пикселей от черного к белому (от 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
    2. изображение этого градиента после гамма-коррекции с коэффициентом из интервала 2.2-2.4

### Результаты
![Result 1](./result3px.png)\
Гамма-коррекция градиента с шагом в 3 пикселя\

![Result 2](./result30px.png)\
Гамма-коррекция градиента с шагом в 30 пикселей\

### Код программы 
```c++
    #include <iostream>
    #include <opencv2/core.hpp>
    #include <opencv2/highgui.hpp>
    #include <opencv2/imgproc.hpp>

    using cv::Mat;
    using cv::Rect;


    Mat part1(double alpha, double beta) {
	    Mat A = Mat::zeros(120, 768, CV_8UC1);
	    for (int i = 0; i < 256; i++) {
		    Rect rect(i * 3, 0, 3, 60);
		    Mat image_roi = A(rect);
		    image_roi.setTo(i);

		    Rect rect1(i * 3, 60, 3, 60);
		    Mat image_roi1 = A(rect1);
		    image_roi1.setTo(alpha * i + beta);
	    }
	    return A;
    }

    Mat part2(double alpha, double beta) {
	    Mat B = Mat::zeros(120, 768, CV_8UC1);
	    for (int i = 0; i <= 768 / 30; i++) {
		    Rect rect = (i == 768 / 30) ? Rect(i * 30, 0, (768 % 30), 60) : Rect(i * 30, 0, 30, 60);
		    Mat image_roi = B(rect);
		    double color = (i == 768 / 30) ?  255 : 255 * (i * 30) / 768;
		    image_roi.setTo(color);

		    Rect rect1 = (i == 768 / 30) ? Rect(i * 30, 60, (768 % 30), 60) : Rect(i * 30, 60, 30, 60);
		    Mat image_roi1 = B(rect1);
		    double color1 = (i == 768 / 30) ?  255 : 255 * (i * 30) / 768;
		    image_roi1.setTo(color1);
	    }
	    return B;
    }

    void Lab1() {
	    double alpha, beta;
	    alpha = 0.9;
	    beta = 0.1;

	    Mat A = part1(alpha, beta);
	    imwrite("result3px.png", A);

	    Mat B = part2(alpha, beta);
	    imwrite("result30px.png", B);

	    Mat final;
	    cv::vconcat(A, B, final);
	    imshow("x", final);
	    cv::waitKey(0);
    }

    int main() {
	    Lab1();
    }
``` 
