## Работа 3. Обработка изображения в градациях серого
автор: Дмитриев А.Д.\
url: https://gitlab.com/DmitrievAD


### Задание
1. Подобрать и зачитать небольшое изображение $S$ в градациях серого.
2. Построить и нарисовать гистограмму $H_s$ распределения яркости пикселей исходного изображения.
3. Сгенерировать табличную функцию преобразования яркости. Построить график $V$ табличной функции преобразования яркости.
4. Применить табличную функцию преобразования яркости к исходному изображению и получить $L$, нарисовать гистограмму $H_L$ преобразованного изображения.
5. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения $C_i$ и их гистограммы $H_{C_{i}}$).
6. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию). Визуализировать на одном изображении исходное $S$ и бинаризованное $B_G$ изображения.
7. Реализовать метод локальной бинаризации. Визуализировать на одном изображении исходное $S$ и бинаризованное $B_L$ изображения.
8. Улучшить одну из бинаризаций путем применения морфологических фильтров. Визуализировать на одном изображении бинарное изображение до и после фильтрации $M$.
9. Сделать визуализацию $K$ бинарной маски после морфологических фильтров поверх исходного изображения (могут помочь подсветка цветом и альфа-блендинг).


### Результаты

![](results/result1.png)\
Рис. 1. Исходное полутоновое изображение $S$

![](results/result2.png)\
Рис. 2. Гистограмма $H_s$ исходного полутонового изображения $S$

![](results/result3.png)\
Рис. 3. Визуализация функции преобразования $V$

![](results/result41.png)\
Рис. 4.1. Таблично пребразованное изображение $L$

![](results/result42.png)\
Рис. 4.2. Гистограмма $H_L$ таблично-пребразованного изображения $L$

![](results/result51.png)\
Рис. 5.1. Преобразование $C_1$ CLAHE с параметрами clipLimit = 40, tileGridSize = (1, 1)

![](results/result52.png)\
Рис. 5.2. Гистограмма $H_{C_{1}}$

![](results/result53.png)\
Рис. 5.3. Преобразование $C_2$ CLAHE с параметрами clipLimit = 40, tileGridSize = (8, 8)

![](results/result54.png)\
Рис. 5.4. Гистограмма $H_{C_{2}}$

![](results/result55.png)\
Рис. 5.5. Преобразование $C_3$ CLAHE с параметрами clipLimit = 40, tileGridSize = (16, 16)

![](results/result56.png)\
Рис. 5.6. Гистограмма $H_{C_{3}}$

![](results/result6.png)\
Рис. 6. Изображение $S$ до и $B_G$ после глобальной бинаризации

![](results/result7.png)\
Рис. 7. Изображение $S$ до и $B_L$ после локальной бинаризации

![](results/result8.png)\
Рис. 8. До и после морфологической фильтрации $M$

![](results/result9.png)\
Рис. 9. Визуализация маски $K$

### Текст программы

```cpp
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <map>
#include <vector>
#include <string>

using cv::Mat;
using cv::Rect;
using std::map;
using std::vector;
using std::string;


string result_path = "results/";

map<int, int> calculate_pixel_count(const Mat& result) {
    map<int, int> brights;
    for (int i = 0; i < result.cols; i++) {
        for (int j = 0; j < result.rows; j++) {
            int color = result.at<uint8_t>(j, i);
            if (brights.count(color) > 0) {
                brights[color] += 1;
            } else {
                brights[color] = 1;
            }
        }
    }
    return brights;
}

Mat build_hist(const Mat& result, int hist_width, int hist_height, int colorbar_height) {

    Mat hist = Mat::zeros(hist_height, hist_width, CV_8UC1);
    hist.setTo(255);
    map<int, int> brights = calculate_pixel_count(result);

    int max_count = 0;
    for (const auto& [color, count] : brights) {
        if (count > max_count) {
            max_count = count;
        }
    }

    int i = 0;
    for (const auto& [color, count] : brights) {
        double multiplier = ((double)count / max_count);
        int height = (int)(hist_height * multiplier);
        Rect roi = Rect(color * 3, hist_height - height, 3, height);
        hist(roi).setTo(0);
    }

    Mat colors = Mat::zeros(colorbar_height, hist_width, CV_8UC1);
    for (int i = 0; i < hist_width; i += 3) {
        Rect roi = Rect(i, 0, 3, colorbar_height);
        int color = ((double)i / hist_width) * 255;
        colors(roi).setTo(color);
    }

    Mat result_hist;
    cv::vconcat(hist, colors, result_hist);

    return result_hist;
}

template <class T>
T uint8_to_uint8_function(T arg) {
    return 255 - arg;
}

Mat plot() {

    vector<uint8_t> x;
    vector<uint8_t> y;
    for (int i = 0; i < 256; i++) {
        x.push_back((uint8_t)i);
        y.push_back((uint8_t)uint8_to_uint8_function(i));
    }

    if (x.size() != y.size()) {
        throw std::invalid_argument("x size must be equal to y size");
    }

    uint8_t max_x = *std::max_element(x.begin(), x.end());
    uint8_t max_y = *std::max_element(y.begin(), y.end());

    Mat plot = Mat::zeros(max_y, max_x, CV_8UC1);
    plot.setTo(255);

    for (int i = 0; i < x.size(); i++) {
        plot.at<uint8_t>(255 - y[i], x[i]) = 0;
    }

    return plot;
}

void get_pointwise_product(const Mat& mat1, const Mat& mat2, Mat& res) {
    if (mat1.cols != mat2.cols || mat1.rows != mat2.rows) {
        throw std::invalid_argument("incorrect shape");
    }
    std::cout << (int)*std::max_element(mat1.begin<uchar>(), mat1.end<uchar>()) << std::endl;
    res = Mat::zeros(mat1.rows, mat1.cols, CV_8UC1);
    for (int i = 0; i < mat1.rows; i++) {
        for (int j = 0; j < mat1.cols; j++) {
            int pix1 = (int)mat1.at<uchar>(i, j);
            int pix2 = (int)mat2.at<uchar>(i, j);
            //std::cout << pix1 << " " << pix2 << std::endl;
            if (pix1 == 0 && pix2 == 0) {
                res.at<uchar>(i, j) = 0;
            } else {
                res.at<uchar>(i, j) = 255;
            }
        }
    }
}

int main() {
    // #1
    Mat img = cv::imread("image.png", cv::IMREAD_GRAYSCALE);
    cv::imwrite(result_path + "result1.png", img);
    // #2
    Mat hist = build_hist(img, 256 * 3, 180, 20);
    cv::imwrite(result_path + "result2.png", hist);
    //cv::imshow("hist", hist);
    // #3
    Mat func = plot();
    cv::imwrite(result_path + "result3.png", func);
    //cv::imshow("func", func);

    // #4
    Mat map_img = uint8_to_uint8_function(img);
    Mat map_hist = build_hist(map_img, 256 * 3, 180, 20);
    cv::imwrite(result_path + "result41.png", map_img);
    cv::imwrite(result_path + "result42.png", map_hist);
    //cv::imshow("map_hist", map_hist);

    // #5
    Mat result1, result2, result3;

    auto map1 = cv::createCLAHE(40, cv::Size(1, 1));
    map1->apply(img, result1);
    cv::imwrite(result_path + "result51.png", result1);

    Mat hist1 = build_hist(result1, 256 * 3, 180, 20);
    cv::imwrite(result_path + "result52.png", hist1);

    auto map2 = cv::createCLAHE(40, cv::Size(8, 8));
    map2->apply(img, result2);
    cv::imwrite(result_path + "result53.png", result2);

    Mat hist2 = build_hist(result2, 256 * 3, 180, 20);
    cv::imwrite(result_path + "result54.png", hist2);

    auto map3 = cv::createCLAHE(40, cv::Size(16, 16));
    map3->apply(img, result3);
    cv::imwrite(result_path + "result55.png", result3);

    Mat hist3 = build_hist(result3, 256 * 3, 180, 20);
    cv::imwrite(result_path + "result56.png", hist3);

    Mat result;
    cv::hconcat(vector<Mat>({result1, result2, result3}), result);
    //cv::imshow("result", result);

    Mat result_hist;
    cv::vconcat(vector<Mat>({hist1, hist2, hist3}), result_hist);
    //cv::imshow("result_hist", result_hist);

    // #6
    Mat bin_img, bin_img_1;
    cv::threshold(img, bin_img, 165, 255, cv::THRESH_BINARY);
    cv::hconcat(img, bin_img, bin_img_1);
    cv::imwrite(result_path + "result6.png", bin_img_1);
    //cv::imshow("bin_img", bin_img_1);

    // #7
    Mat loc_bin_img, loc_bin_img_1;
    cv::adaptiveThreshold(img, loc_bin_img, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 19, 0);
    cv::hconcat(img, loc_bin_img, loc_bin_img_1);
    cv::imwrite(result_path + "result7.png", loc_bin_img_1);
    cv::imshow("loc_bin_img", loc_bin_img_1);

    // #8
    Mat morph_res;
    Mat kernel = Mat::ones(3, 3, CV_8UC1);
    cv::dilate(loc_bin_img, morph_res, kernel);
    cv::dilate(morph_res, morph_res, kernel);
    
    cv::erode(morph_res, morph_res, kernel);
    cv::erode(morph_res, morph_res, kernel);
    cv::erode(morph_res, morph_res, kernel);
    cv::erode(morph_res, morph_res, kernel);
    cv::erode(morph_res, morph_res, kernel);
    cv::erode(morph_res, morph_res, kernel);
    cv::erode(morph_res, morph_res, kernel);
    Mat r, concat_r;
    get_pointwise_product(morph_res, loc_bin_img, r);
    cv::hconcat(loc_bin_img, r, concat_r);
    cv::imwrite(result_path + "result8.png", concat_r);
    cv::imshow("loc_bin_img_morph", concat_r);

    // #9
    Mat source, mask;
    source = loc_bin_img / 2;
    mask = morph_res / 2;
    Mat mask_res = source + mask;
    cv::imwrite(result_path + "result9.png", mask_res);
    cv::imshow("mask", mask_res);

    cv::waitKey(0);
}
```
