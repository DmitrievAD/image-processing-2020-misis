## Работа 4. Использование частных производных для выделения границ
автор: Дмитриев А.Д.\
url: https://gitlab.com/DmitrievAD

### Задание
1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата $F_1$.
3. Применить второй линейный фильтр и сделать визуализацию результата $F_2$.
4. Вычислить $R=\sqrt{F_1^2 + F_2^2}$  и сделать визуализацию R.

### Результаты

![](results/source_image.png)\
Рис. 1. Исходное тестовое изображение

![](results/first_filtred.png)\
Рис. 2. Визуализация результата $F_1$ применения фильтра

![](results/second_filtred.png)\
Рис. 3. Визуализация результата $F_2$ применения фильтра

![](results/third_filtred.png)\
Рис. 4. Визуализация модуля градиента $R$

### Текст программы
```c++
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <string>

using cv::Mat;
using cv::Point;
using cv::circle;
using cv::hconcat;
using cv::waitKey;
using cv::vconcat;
using cv::filter2D;
using cv::pow;
using cv::sqrt;
using std::vector;
using std::string;

vector<vector<float>> kernel_matrix = {
    {-1, 0, 1},
    {-2, 0, 2},
    {-1, 0, 1}
};

string image_dir = "results/";

Mat get_kernel(bool transposed=false) {
    Mat kernel(3, 3, CV_32F);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (transposed) {
                kernel.at<float>(i, j) = kernel_matrix[j][i];
            } else {
                kernel.at<float>(i, j) = kernel_matrix[i][j];
            }
        }
    }

    return kernel;
}

void assignment1() {
    vector<double> colors = {0, 0.5, 1};

    int type = CV_32F;
    Mat r1(200, 200, type), r2(200, 200, type), r3(200, 200, type), 
        r4(200, 200, type), r5(200, 200, type), r6(200, 200, type);

    vector<int> bcolor_indexes = {0, 1, 2, 2, 0, 1};
    vector<int> ccolor_indexes = {2, 0, 1, 0, 1, 2};
    vector<Mat> pieces = {r1, r2, r3, r4, r5, r6};
    
    for (int i = 0; i< 6; i++) {
        pieces[i].setTo(colors[bcolor_indexes[i]]);
        circle(pieces[i], Point(100, 100), 80, colors[ccolor_indexes[i]], 5);
    }

    vector<Mat> top_v = {r1, r2, r3};
    vector<Mat> bottom_v = {r4, r5, r6};

    Mat top;
    hconcat(top_v, top);

    Mat bottom;
    hconcat(bottom_v, bottom);

    Mat result;
    vconcat(top, bottom, result);

    imshow("result", result);
    imwrite(image_dir + "source_image.png", 255 * result);
    
    Mat kernel1 = get_kernel();
    Mat first_filtred;
    filter2D(result, first_filtred, -1, kernel1, Point(-1, -1), 0, cv::BORDER_DEFAULT);
    first_filtred += 4;
    first_filtred /= 8;

    imshow("first_filtred", first_filtred);
    imwrite(image_dir + "first_filtred.png", 255 * first_filtred);


    Mat kernel2 = get_kernel(true);
    Mat second_filtred;
    filter2D(result, second_filtred, -1, kernel2, Point(-1, -1), 0, cv::BORDER_DEFAULT);
    second_filtred += 4;
    second_filtred /= 8;

    imshow("second_filtred", second_filtred);
    imwrite(image_dir + "second_filtred.png", 255 * second_filtred);

    Mat squared_res1, squared_res2, third_filtred;
    pow(first_filtred, 2, squared_res1);
    pow(second_filtred, 2, squared_res2);
    sqrt(squared_res1 + squared_res2, third_filtred);

    imshow("third_filtred", third_filtred);
    imwrite(image_dir + "third_filtred.png", 255 * third_filtred);
    
    waitKey(0);
}

int main() {
    assignment1();
}
```
